// node-resolve will resolve all the node dependencies
import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

import rollUpPostcss from 'rollup-plugin-postcss';
import autoprefixer from 'autoprefixer';

export default {
  input: './src/Button.js',
  output: {
    file: 'dist/componente.js',
    format: 'cjs'
  },
  // All the used libs needs to be here
  external: [
    'react',
    'react-proptypes'
  ],
  plugins: [
    resolve(), //Resolve node dependencies
    babel({
      exclude: 'node_modules/**'
    }),
    rollUpPostcss({plugins: [autoprefixer()], extract: './dist/styles.css'})
  ]
};